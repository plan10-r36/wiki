# Wiki – Personal Wiki

This is a local wiki you edit and manage just using the commandline.

## License
See LICENSE.

## Dependencies
* git for change management

## Installation

	% git clone git://r-36.net/wiki
	% cd wiki
	% cp wiki $HOME/bin
	% mkdir -p $HOME/.psw
	# psw – personal wiki
	% cd $HOME/.psw
	% git init

## The Base
If  you  want  to have different wikis, use the ‐b parameter to define a
different directory where the wiki pages are managed. The default is the
above mentioned »~/.psw«.

	% wiki -b $HOME/.someotherwiki somecommand

Of course do not forget to git init this directory.

## Add A Page

	% wiki -e page

## Delete A Page

	% wiki -d page

## Search The Pages

	% wiki -s searchterm

## Rename Some page

	% wiki -r fromname toname

## Display Page To Stdout

	% wiki -o page

## List All Pages

	% wiki -l

## Commit If Manual Changes
If you changed something by hand in the wiki directory, run:

	% wiki -c


## Commandline Completion

	% wiki -e <tab>

For a way to handle this, see the »completion« directory.



## Bug Reports

	All to:

		Christoph Lohmann <20h@r-36.net>

Have fun!

20h

